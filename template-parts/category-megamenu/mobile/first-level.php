<div class="gf-category-accordion__item gf-category-accordion__item--main">
    <a tabindex="-1" href="<?=user_trailingslashit(get_term_link((int)$cat->term_id))?>"><?=$cat->name?></a>
    <i class="openMoreCategories fas fa-plus"></i>
